/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.dbmem;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class position {

    private int location;
    private String description;
    private int posid;
    private int sensorid_id;
    private int mesid;

    /**
     * @return the location
     */
    public int getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(int location) {
        this.location = location;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the posid
     */
    public int getPosid() {
        return posid;
    }

    /**
     * @param posid the posid to set
     */
    public void setPosid(int posid) {
        this.posid = posid;
    }

    /**
     * @return the sensorid_id
     */
    public int getSensorid_id() {
        return sensorid_id;
    }

    /**
     * @param sensorid_id the sensorid_id to set
     */
    public void setSensorid_id(int sensorid_id) {
        this.sensorid_id = sensorid_id;
    }

    /**
     * @return the mesid
     */
    public int getMesid() {
        return mesid;
    }

    /**
     * @param mesid the mesid to set
     */
    public void setMesid(int mesid) {
        this.mesid = mesid;
    }
}
