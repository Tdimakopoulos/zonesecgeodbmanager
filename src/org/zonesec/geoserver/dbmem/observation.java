/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.dbmem;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class observation {

    private int geoid;
    private int observationid;
    private int posid;
    private int sensorid_id;
    private String description;
    private int observationtype;
    private String sequence;

    /**
     * @return the geoid
     */
    public int getGeoid() {
        return geoid;
    }

    /**
     * @param geoid the geoid to set
     */
    public void setGeoid(int geoid) {
        this.geoid = geoid;
    }

    /**
     * @return the observationid
     */
    public int getObservationid() {
        return observationid;
    }

    /**
     * @param observationid the observationid to set
     */
    public void setObservationid(int observationid) {
        this.observationid = observationid;
    }

    /**
     * @return the posid
     */
    public int getPosid() {
        return posid;
    }

    /**
     * @param posid the posid to set
     */
    public void setPosid(int posid) {
        this.posid = posid;
    }

    /**
     * @return the sensorid_id
     */
    public int getSensorid_id() {
        return sensorid_id;
    }

    /**
     * @param sensorid_id the sensorid_id to set
     */
    public void setSensorid_id(int sensorid_id) {
        this.sensorid_id = sensorid_id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the observationtype
     */
    public int getObservationtype() {
        return observationtype;
    }

    /**
     * @param observationtype the observationtype to set
     */
    public void setObservationtype(int observationtype) {
        this.observationtype = observationtype;
    }

    /**
     * @return the sequence
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
}
