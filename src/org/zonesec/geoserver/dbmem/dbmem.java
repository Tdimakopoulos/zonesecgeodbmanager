/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.dbmem;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.zonesec.geoserver.db.connectionmanager.connectionmanager;
import org.zonesec.geoserver.db.insert.InsertManager;
import org.zonesec.geoserver.db.statementmanager.statementmanager;


/**
 *
 * @author Thomas Dimakopoulos
 */
public class dbmem implements Serializable {

    private List<geoinformation> pgeoinfo=new ArrayList();
    private List<measurment> pmeasurments=new ArrayList();
    private List<observation> pobservations=new ArrayList();
    private List<position> ppositions=new ArrayList();
    private List<sensor> psensors=new ArrayList();
    
    private Connection c;
    private Statement stmt;
    private connectionmanager pcm = new connectionmanager();
    private statementmanager psm = new statementmanager();

    public void Connect() {

        if (c == null) {
            pcm.ConnectTODB();
            c = pcm.getC();
        }
        if (stmt == null) {
            psm.CreateStatement(c);
            stmt = psm.getStmt();
        }
    }

    public void CloseAll() {
        if (stmt != null) {
            psm.CloseStatement();
        }
        if (c != null) {
            pcm.CloseDB();
        }

    }

    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // load into arrays

    public void Loadgeoinformation() {
        if (c == null) {
            pcm.ConnectTODB();
            c = pcm.getC();
        }
        if (stmt == null) {
            psm.CreateStatement(c);
            stmt = psm.getStmt();
        }
        
        pgeoinfo.clear();
        try {
            c.setAutoCommit(false);
            ResultSet rs = stmt.executeQuery("SELECT \"latitudemid\",\"longitudemid\",\"orientationmid\",\"elevationmid\",\"geoid\" FROM \"public\".\"geoinformation\";");
            while (rs.next()) {

                geoinformation item=new geoinformation();
                
                item.setElevationmid(rs.getFloat("elevationmid"));
                item.setGeoid(rs.getInt("geoid"));
                item.setLatitudemid(rs.getFloat("latitudemid"));
                item.setLongitudemid(rs.getFloat("longitudemid"));
                item.setOrientationmid(rs.getFloat("orientationmid"));
                pgeoinfo.add(item);

            }
            rs.close();
            System.out.println("Load GeoInfo success !!!");
        } catch (Exception e) {
            System.out.println("Error on Load GeoInfo" + e.getMessage());
        }
    }

    
    public void Loadmeasurments() {
        if (c == null) {
            pcm.ConnectTODB();
            c = pcm.getC();
        }
        if (stmt == null) {
            psm.CreateStatement(c);
            stmt = psm.getStmt();
        }
        
        pmeasurments.clear();
        
        try {
            c.setAutoCommit(false);
            ResultSet rs = stmt.executeQuery("SELECT \"uom\",\"value\",\"mid\" FROM \"public\".\"measurment\";");
            while (rs.next()) {

                measurment item=new measurment();
                
                
                item.setMid(rs.getInt("mid"));
                item.setUom(rs.getString("uom"));
                item.setValue(rs.getFloat("value"));
                pmeasurments.add(item);

            }
            rs.close();
            System.out.println("Load Measurments success !!!");
        } catch (Exception e) {
            System.out.println("Error on Load measurments" + e.getMessage());
        }
    }
    
    
    public void Loadobservation() {
        if (c == null) {
            pcm.ConnectTODB();
            c = pcm.getC();
        }
        if (stmt == null) {
            psm.CreateStatement(c);
            stmt = psm.getStmt();
        }
        
        pobservations.clear();
        
        
        try {
            c.setAutoCommit(false);
            ResultSet rs = stmt.executeQuery("SELECT  \"geoid\",\"observationid\",\"posid\",\"sensorid_id\",\"description\",\"observationtype\",\"sequence\" FROM \"public\".\"observation\";");
            while (rs.next()) {

                observation item=new observation();
                
                item.setDescription(rs.getString("description"));
                item.setSequence(rs.getString("sequence"));
                item.setGeoid(rs.getInt("geoid"));
                item.setObservationid(rs.getInt("observationid"));
                item.setObservationtype(rs.getInt("observationtype"));
                item.setPosid(rs.getInt("posid"));
                item.setSensorid_id(rs.getInt("sensorid_id"));
                
                pobservations.add(item);

            }
            rs.close();
            System.out.println("Load Observations success !!!");
        } catch (Exception e) {
            System.out.println("Error on Load Observations" + e.getMessage());
        }
    }
    
    
    public void Loadposition() {
        if (c == null) {
            pcm.ConnectTODB();
            c = pcm.getC();
        }
        if (stmt == null) {
            psm.CreateStatement(c);
            stmt = psm.getStmt();
        }
        
        ppositions.clear();
        
        try {
            c.setAutoCommit(false);
            ResultSet rs = stmt.executeQuery("SELECT \"location\",\"description\",\"posid\",\"sensorid_id\",\"mesid\" FROM \"public\".\"position\";");
            while (rs.next()) {

                position item=new position();
                
                item.setPosid(rs.getInt("posid"));
                item.setSensorid_id(rs.getInt("sensorid_id"));
                item.setDescription(rs.getString("description"));
                item.setLocation(rs.getInt("location"));
                item.setMesid(rs.getInt("mesid"));
                ppositions.add(item);

            }
            rs.close();
            System.out.println("Load Positions success !!!");
        } catch (Exception e) {
            System.out.println("Error on Load positions" + e.getMessage());
        }
    }
    
    public void Loadsensor() {
        if (c == null) {
            pcm.ConnectTODB();
            c = pcm.getC();
        }
        if (stmt == null) {
            psm.CreateStatement(c);
            stmt = psm.getStmt();
        }
        
        psensors.clear();
        
        try {
            c.setAutoCommit(false);
            ResultSet rs = stmt.executeQuery("SELECT \"sensorid\",\"sensortype\",\"timestamp\",\"sensor_id\" FROM \"public\".\"sensor\";");
            while (rs.next()) {

                sensor item=new sensor();
                
                
                item.setSensor_id(rs.getInt("sensor_id"));
                item.setSensorid(rs.getString("sensorid"));
                item.setSensortype(rs.getString("sensortype"));
                item.setTimestamp(rs.getString("timestamp"));
                psensors.add(item);

            }
            rs.close();
            System.out.println("Load Sensors success !!!");
        } catch (Exception e) {
            System.out.println("Error on Load Sensors" + e.getMessage());
        }
    }

    /**
     * @return the pgeoinfo
     */
    public List<geoinformation> getPgeoinfo() {
        return pgeoinfo;
    }

    /**
     * @param pgeoinfo the pgeoinfo to set
     */
    public void setPgeoinfo(List<geoinformation> pgeoinfo) {
        this.pgeoinfo = pgeoinfo;
    }

    /**
     * @return the pmeasurments
     */
    public List<measurment> getPmeasurments() {
        return pmeasurments;
    }

    /**
     * @param pmeasurments the pmeasurments to set
     */
    public void setPmeasurments(List<measurment> pmeasurments) {
        this.pmeasurments = pmeasurments;
    }

    /**
     * @return the pobservations
     */
    public List<observation> getPobservations() {
        return pobservations;
    }

    /**
     * @param pobservations the pobservations to set
     */
    public void setPobservations(List<observation> pobservations) {
        this.pobservations = pobservations;
    }

    /**
     * @return the ppositions
     */
    public List<position> getPpositions() {
        return ppositions;
    }

    /**
     * @param ppositions the ppositions to set
     */
    public void setPpositions(List<position> ppositions) {
        this.ppositions = ppositions;
    }

    /**
     * @return the psensors
     */
    public List<sensor> getPsensors() {
        return psensors;
    }

    /**
     * @param psensors the psensors to set
     */
    public void setPsensors(List<sensor> psensors) {
        this.psensors = psensors;
    }

    /**
     * @return the c
     */
    public Connection getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(Connection c) {
        this.c = c;
    }

    /**
     * @return the stmt
     */
    public Statement getStmt() {
        return stmt;
    }

    /**
     * @param stmt the stmt to set
     */
    public void setStmt(Statement stmt) {
        this.stmt = stmt;
    }

    /**
     * @return the pcm
     */
    public connectionmanager getPcm() {
        return pcm;
    }

    /**
     * @param pcm the pcm to set
     */
    public void setPcm(connectionmanager pcm) {
        this.pcm = pcm;
    }

    /**
     * @return the psm
     */
    public statementmanager getPsm() {
        return psm;
    }

    /**
     * @param psm the psm to set
     */
    public void setPsm(statementmanager psm) {
        this.psm = psm;
    }
    
}
    
    