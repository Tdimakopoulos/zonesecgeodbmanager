/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.dbmem;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class sensor {

    private String sensorid;
    private String sensortype;
    private String timestamp;
    private int sensor_id;

    /**
     * @return the sensorid
     */
    public String getSensorid() {
        return sensorid;
    }

    /**
     * @param sensorid the sensorid to set
     */
    public void setSensorid(String sensorid) {
        this.sensorid = sensorid;
    }

    /**
     * @return the sensortype
     */
    public String getSensortype() {
        return sensortype;
    }

    /**
     * @param sensortype the sensortype to set
     */
    public void setSensortype(String sensortype) {
        this.sensortype = sensortype;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the sensor_id
     */
    public int getSensor_id() {
        return sensor_id;
    }

    /**
     * @param sensor_id the sensor_id to set
     */
    public void setSensor_id(int sensor_id) {
        this.sensor_id = sensor_id;
    }
}
