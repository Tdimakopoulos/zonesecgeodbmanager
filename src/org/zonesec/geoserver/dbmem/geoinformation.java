/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.dbmem;

/**
 *
 * @author Thomas Dimakopoulos
 
 Geo Information (Position and ID)
 
 * 
 */
public class geoinformation {

    private float latitudemid;
    private float longitudemid;
    private float orientationmid;
    private float elevationmid;
    private int geoid;

    /**
     * @return the latitudemid
     */
    public float getLatitudemid() {
        return latitudemid;
    }

    /**
     * @param latitudemid the latitudemid to set
     */
    public void setLatitudemid(float latitudemid) {
        this.latitudemid = latitudemid;
    }

    /**
     * @return the longitudemid
     */
    public float getLongitudemid() {
        return longitudemid;
    }

    /**
     * @param longitudemid the longitudemid to set
     */
    public void setLongitudemid(float longitudemid) {
        this.longitudemid = longitudemid;
    }

    /**
     * @return the orientationmid
     */
    public float getOrientationmid() {
        return orientationmid;
    }

    /**
     * @param orientationmid the orientationmid to set
     */
    public void setOrientationmid(float orientationmid) {
        this.orientationmid = orientationmid;
    }

    /**
     * @return the elevationmid
     */
    public float getElevationmid() {
        return elevationmid;
    }

    /**
     * @param elevationmid the elevationmid to set
     */
    public void setElevationmid(float elevationmid) {
        this.elevationmid = elevationmid;
    }

    /**
     * @return the geoid
     */
    public int getGeoid() {
        return geoid;
    }

    /**
     * @param geoid the geoid to set
     */
    public void setGeoid(int geoid) {
        this.geoid = geoid;
    }

}
