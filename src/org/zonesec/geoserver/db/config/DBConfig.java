/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.db.config;

import java.io.Serializable;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class DBConfig  implements Serializable {
//jdbc:postgresql://193.92.114.117:5432/geoserver
    private String DBURL = "193.92.114.117";
    private String DBPORT = "5432";
    private String DBNAME = "geoserver";
    private String DBUSER = "geoserver";
    private String DBPASSWD = "geo";

    /**
     * @return the DBURL
     */
    public String getDBURL() {
        return DBURL;
    }

    /**
     * @param DBURL the DBURL to set
     */
    public void setDBURL(String DBURL) {
        this.DBURL = DBURL;
    }

    /**
     * @return the DBPORT
     */
    public String getDBPORT() {
        return DBPORT;
    }

    /**
     * @param DBPORT the DBPORT to set
     */
    public void setDBPORT(String DBPORT) {
        this.DBPORT = DBPORT;
    }

    /**
     * @return the DBNAME
     */
    public String getDBNAME() {
        return DBNAME;
    }

    /**
     * @param DBNAME the DBNAME to set
     */
    public void setDBNAME(String DBNAME) {
        this.DBNAME = DBNAME;
    }

    /**
     * @return the DBUSER
     */
    public String getDBUSER() {
        return DBUSER;
    }

    /**
     * @param DBUSER the DBUSER to set
     */
    public void setDBUSER(String DBUSER) {
        this.DBUSER = DBUSER;
    }

    /**
     * @return the DBPASSWD
     */
    public String getDBPASSWD() {
        return DBPASSWD;
    }

    /**
     * @param DBPASSWD the DBPASSWD to set
     */
    public void setDBPASSWD(String DBPASSWD) {
        this.DBPASSWD = DBPASSWD;
    }
    
    
}
