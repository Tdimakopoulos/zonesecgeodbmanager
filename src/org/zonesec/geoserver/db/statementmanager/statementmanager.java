/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.db.statementmanager;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class statementmanager  implements Serializable {
    
    private Statement stmt = null;
    
    public boolean CreateStatementi(Connection c)
    {
        
        try {
            c.setAutoCommit(true);
            stmt = c.createStatement();
            return true;
        } catch (SQLException ex) {
            System.out.println("org.crisishub.db.statementmanager.statementmanager.CreateStatement() : "+ ex.getMessage());
            return false;
        }
    }
    
    public boolean CreateStatement(Connection c)
    {
        
        try {
     
            stmt = c.createStatement();
            return true;
        } catch (SQLException ex) {
            System.out.println("org.crisishub.db.statementmanager.statementmanager.CreateStatement() : "+ ex.getMessage());
            return false;
        }
    }
    
    public boolean CloseStatement()
    {
        try {
            if(stmt!=null)
                stmt.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("org.crisishub.db.statementmanager.statementmanager.CreateStatement() : "+ ex.getMessage());
            return false;
        }
    }       

    public boolean ExecuteUpdate(String sql)
    {
        
        try {
            stmt.executeUpdate(sql);
            return true;
        } catch (SQLException ex) {
            System.out.println("org.crisishub.db.statementmanager.statementmanager.CreateStatement() : "+ ex.getMessage());
            return false; 
        }
    }
    /**
     * @return the stmt
     */
    public Statement getStmt() {
        return stmt;
    }
            
}
