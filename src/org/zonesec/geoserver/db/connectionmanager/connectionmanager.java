/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.db.connectionmanager;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.zonesec.geoserver.db.config.DBConfig;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class connectionmanager  implements Serializable {

    private Connection c = null;
    
    private DBConfig pconfig=new DBConfig();
    
    public boolean CloseDB() {
        try {
            if(c!=null)
                c.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("crisishubdbmanager.CrisisHubDBManager.CloseDB() : " + ex.getMessage());
            return false;
        }
    }

    public boolean ConnectTODB() {
        try {
            //jdbc:postgresql://193.92.114.117:5432/geoserver
            Class.forName("org.postgresql.Driver").newInstance();
            c = DriverManager
                    .getConnection("jdbc:postgresql://"+pconfig.getDBURL()+":"+pconfig.getDBPORT()+"/"+pconfig.getDBNAME(), pconfig.getDBUSER(), pconfig.getDBPASSWD());
            
            System.out.println("Opened database successfully");
            return true;
        } catch (ClassNotFoundException ex) {
            System.out.println("crisishubdbmanager.CrisisHubDBManager.ConnectTODB() : " + ex.getMessage());
            return false;
        } catch (SQLException ex) {
            System.out.println("crisishubdbmanager.CrisisHubDBManager.ConnectTODB() : " + ex.getMessage());
            return false;
        } catch (InstantiationException ex) {
            System.out.println("crisishubdbmanager.CrisisHubDBManager.ConnectTODB() : " + ex.getMessage());
            return false;
        } catch (IllegalAccessException ex) {
            System.out.println("crisishubdbmanager.CrisisHubDBManager.ConnectTODB() : " + ex.getMessage());
            return false;
        }
    }

    /**
     * @return the c
     */
    public Connection getC() {
        return c;
    }

}
