/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.db.insert;

import java.io.Serializable;
import org.zonesec.geoserver.db.connectionmanager.connectionmanager;
import org.zonesec.geoserver.db.statementmanager.statementmanager;


/**
 *
 * @author Thomas Dimakopoulos
 */
public class InsertManager implements Serializable {


    public void AddSensor(String sensorid,String sensortype,int timestamp,int sensor_id) {
        String psql = "INSERT INTO \"public\".\"sensor\" (sensorid,sensortype,timestamp,sensor_id) VALUES "
                + "("+sensorid+",'"+sensortype+"','"+timestamp+"','"+sensor_id+"');";
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(psql);
        psm.CloseStatement();
        pp.CloseDB();
    }

    public void AddGeoInformation(float latitudemid,float longitudemid,float orientationmid,float elevationmid,int geoid) {
        String psql = "INSERT INTO \"public\".\"geoinformation\" (latitudemid,longitudemid,orientationmid,elevationmid,geoid) VALUES ("+latitudemid+","+longitudemid+","+orientationmid+","
                + elevationmid+","+geoid+");";
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(psql);
        psm.CloseStatement();
        pp.CloseDB();
    }

    
    public void Addmeasurments(String uom,float value,int mid) {
        String psql = "INSERT INTO \"public\".\"measurment\" (uom,value,mid) VALUES "
                + "('"+uom+"',"+value+","+mid+");";
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(psql);
        psm.CloseStatement();
        pp.CloseDB();
    }
    
    public void Addposition(int mesid,int location,int description,int posid,int sensorid_id) {
        String psql = "INSERT INTO \"public\".\"position\" (mesid,location,description,posid,sensorid_id) VALUES "
                + "("+mesid+","+location+","+description+","+posid+","+sensorid_id+");";
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(psql);
        psm.CloseStatement();
        pp.CloseDB();
    }
    
        public void AddObservation(int geoid,int observationid,int posid,int sensorid_id,String description,int observationtype,String sequence) {
        String psql = "INSERT INTO \"public\".\"observation\" (geoid,observationid,posid,sensorid_id,description,observationtype,sequence) VALUES "
                + "("+geoid+","+observationid+","+posid+","+sensorid_id+",'"+description+"',"+observationtype+",'"+sequence+"');";
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(psql);
        psm.CloseStatement();
        pp.CloseDB();
    }
        
    public void updatemeasurments(int id,float value) {
        String addmsg = "UPDATE \"public\".\"measurment\" SET  value="+value+
                " WHERE mid=" + id;
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(addmsg);
        psm.CloseStatement();
        pp.CloseDB();
    }
    
    public void updategeoinfo(int id,float lat,float lon,float elev,float orin) {
        String addmsg = "UPDATE \"public\".\"geoinformation\" SET  latitudemind="+lat+",longitudemid="+lon+",elevationmid="
        +elev+",orientationmid="+orin+" WHERE geoid=" + id;
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(addmsg);
        psm.CloseStatement();
        pp.CloseDB();
    }
}
